import re


def normalize(string):
    lowercase_string=string.lower()
    no_punctuation_string=remove_punctuation(lowercase_string)
    no_stopword_string=remove_stopword(no_punctuation_string)
    return no_stopword_string


def remove_punctuation(lowercase_string):
    punctuations = {".","'","?","!"}
    no_punctuation_string=""
    for char in lowercase_string:
        if char not in punctuations:
            no_punctuation_string=no_punctuation_string+char
    return no_punctuation_string


def remove_stopword(no_punctuation_string):
    word_list = no_punctuation_string.split()
    stopwords = ["of","the","by","not","this", "a" , "is", "and"]
    no_stopword_string=(' '.join([word for word in word_list if word not in stopwords]))
    return no_stopword_string


def transform(no_stopword_string):
    word_list= no_stopword_string.split(" ")
    keyword_frequency=get_word_frequency(word_list)
    return keyword_frequency


def get_word_frequency(word_list):
    frequency_dictionary={}
    for word in word_list:
        if word in frequency_dictionary:
            frequency_dictionary[word] +=1
        else:
            frequency_dictionary[word] =1
    return frequency_dictionary


def calculate_score(dictionary1,dictionary2):
    score = 0
    for key in dictionary1.keys():
        if key in dictionary2:
            score += dictionary1[key] - dictionary2[key]
        elif key not in dictionary2:
                score+= dictionary1[key]
    return score


def main():
    expected= "this is a test"
    result= "test a is this"
    dictionary_list = []
    for input_string in(expected,result):
        print 'actual string is:--------->', input_string
        normalized_string=normalize(input_string)
        print 'normalized string is:-------> ', normalized_string
        keyword_frequency=transform(normalized_string)
        print 'transformed string is:-------> ', keyword_frequency
        dictionary_list.append(keyword_frequency)
    for dictionary in range(len(dictionary_list) - 1):
        score=calculate_score(dictionary_list[0],dictionary_list[dictionary+1])
        print 'score is', score


if __name__== "__main__":
    main()






