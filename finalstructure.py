def normalize(input):
    lower=input.lower()
    no_punctuation=remove_punctuation(lower)
    no_stopwords=remove_stopwords(no_punctuation)
    return no_stopwords

def remove_punctuation(lower):
  pass

def remove_stopwords(no_punctuation):
  pass

# Transformation functions 
def transform (no_stopword):

 word_list = tokenize(no_stopword)
 freq_dict = get_word_frequency(wordlist)
 return freq_dict

def tokenize(no_stopword):
  pass

def get_word_frequency(wordlist):
  pass  
  
def calculate_score(*transformed_freq_dicts):
    print("actual string is:",original)
    print("result string is:",result)
    print("and the score is",score)

# High level function
def compare_strings(original, result):
    transformed_freq_dicts = []
    original="this is a cat"
    result= "this is a dog"
    for input in [original, result]:
        normalized_string = normalize(input)
        transformed_freq_dicts += transform(normalized_string)
    return calculate_score(*transformed_freq_dicts)	

def main():
    original="this is a test!"
    result= "There's a cat."
    compare(original,result) 

if __name__== "__main__":
  main()					
